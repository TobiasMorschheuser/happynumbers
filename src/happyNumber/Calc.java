package happyNumber;

public class Calc {
	//private variables
	private String str;
	
	//private functions
	private int square(int leftNum) {
		int sum = 0, rightNum = 0;
		while (leftNum > 0) {
			
			rightNum = leftNum % 10;
			sum += rightNum * rightNum;
			leftNum = leftNum / 10;
		}
		return sum;
	}
	
	//construct
	public Calc() {
		this.str = "";
	}
	
	//Getters
	public String getString() {
		return this.str;
	}
	
	//public functions
	public boolean isHappy(int value) {
		int temp = value;
		do {
			value = square(value);
			temp = square(square(temp));
			this.str +=" => " + value;
		} while (value != temp);
		
		if (value == 1) {
			return true;
		}
		
		this.str = "";
		return false;
	}	 
}
