package happyNumber;

////////////////////////////////////
//
// This simple program was made by
// --- Tobias Morschheuser --- 
// ---  Tobias@winux.se ---
//
// -- Time --
// start: 2017-10-19 10.30
// end: 2017-10-19 11.00
//
// start: 2017-10-20 09.51
// end: 2017-10-19 10.53
// total time: 0,30 + 1.02 == 1h 32m
////////////////////////////////////


public class Main {

	public static void main(String[] args) {
		int end = 1000;
		Calc calc = new Calc();
		printHappy(calc, end);
	}//end main(String[] args)

	private static void printHappy(Calc calc, int end) {
		
		for (int i = 1; i <= end; i++) {
		
			if (calc.isHappy(i)) {
				System.out.println(i + calc.getString());
			} //endif
		
		} // endfor
			
	} // end printHappy(Calc calc, int end)

} // end class
